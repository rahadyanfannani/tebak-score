import Phaser from 'phaser-ce';
import { getBanner, register, login } from '../lib/API';

export default class Preload extends Phaser.State {
  preload() {
    this.maxWidth = 1000;
    this.background = this.add.image(this.world.centerX, 0, 'lapangan');
    this.background.anchor.set(0.5, 0);
    this.background.scale.set(this.game.height / this.background.height);
    this.logo = this.add.image(this.world.centerX, 120, 'icon');
    this.logo.anchor.set(0.5, 0);
    this.loading = this.add.image(this.world.centerX, 800, 'loading');
    this.loading.anchor.set(0.5, 0);
    this.loadingBar = this.add.sprite(this.world.centerX - 244, 805, 'loading_bar');
    this.load.setPreloadSprite(this.loadingBar);

    this.load.image('vs', './assets/images/vs.png');
    this.load.image('header', './assets/images/pieces/header.png');
    this.load.image('bni_kiri', './assets/images/pieces/kiri_atas.png');
    this.load.image('bni_kanan', './assets/images/pieces/kanan_atas.png');
    this.load.image('footer', './assets/images/pieces/footer.png');
    this.load.image('logo_footer', './assets/images/pieces/bawah_tengah.png');
    this.load.image('orange_line', './assets/images/pieces/orange_line.png');
    this.load.image('persyaratan', './assets/images/pieces/persyaratan.png');

    this.load.image('editScore', './assets/images/bgeditscore.png');
    this.load.image('left', './assets/images/left.png');
    this.load.image('right', './assets/images/right.png');
    this.load.image('submit', './assets/images/btn_kirim.png');
    this.load.image('leaderboard', './assets/images/leaderboard.png');
    this.load.image('history', './assets/images/history.png');
    this.load.image('slider_left', './assets/images/slider_left.png');
    this.load.image('slider_right', './assets/images/slider_right.png');

    this.load.image('bg_footer', './assets/images/bg_footer.png');
    this.load.image('bg_hitam', './assets/images/bg_hitam.png');
    this.load.image('bg_kalah', './assets/images/bg_kalah.png');
    this.load.image('bg_menang', './assets/images/bg_menang.png');
    this.load.image('bg_putih', './assets/images/bg_putih.png');
    this.load.image('bg_seri', './assets/images/bg_seri.png');
    this.load.image('bgtampilscore', './assets/images/bgtampilscore.png');
    this.load.image('icon_benar', './assets/images/icon_benar.png');
    this.load.image('icon_salah', './assets/images/icon_salah.png');
    this.load.image('popup_choice', './assets/images/popup_choice.png');
    this.load.image('notif_popup', './assets/images/notif_popup.png');
    this.load.image('header_home', './assets/images/header_home.png');
    this.load.image('bg_player', './assets/images/pieces/bg_player.png');
    this.load.image('notif', './assets/images/pieces/notif.png');
    this.load.image('score', './assets/images/score.png');
    this.load.image('btn_back', './assets/images/btn_back.png');
    this.load.image('checkbox', './assets/images/checkbox.png');
    this.load.image('checkbox_false', './assets/images/checkbox_false.png');
    this.load.image('orange', './assets/images/orange.png');
    this.load.image('biru', './assets/images/biru.png');
    this.load.image('akumulatif', './assets/images/akumulatif.png');
    this.load.image('pertandingan_terakhir', './assets/images/pertandingan_terakhir.png');
    this.load.image('tebakan_saya', './assets/images/tebakan_saya.png');
    this.load.image('akumulatif_off', './assets/images/akumulatif_off.png');
    this.load.image('pertandingan_terakhir_off', './assets/images/pertandingan_terakhir_off.png');
    this.load.image('tebakan_saya_off', './assets/images/tebakan_saya_off.png');
  }
  init() {
    this.totalLoad = 0;
  }
  auth(user, name, email, phone) {
    window.user = user;
    register(user, name)
      .then(() => {
        this.loadingBar.width = 440;
        return login(user, email, phone, name);
      })
      .then(({ data }) => {
        this.loadingBar.width = 450;
        window.session_id = data.session_id;
        window.user_id = data.user_id;
        return getBanner();
      })
      .then(({ data }) => {
        window.totalImage = data.banner_data.length;
        let i = 1;
        window.banner = data.banner_data;
        data.banner_data.forEach((val) => {
          this.load.image(`banner${i}`, `http://net-api.agatelabs.com/bni-tebak-skor/${val.banner_url}`);
          i += 1;
        });
        this.load.onFileComplete.add(this.nextState, this);
        this.load.start();
      });
  }
  nextState() {
    if (this.totalLoad === window.totalImage - 1) {
      this.loadingBar2 = this.add.sprite(this.world.centerX - 244, 805, 'loading_bar');
      this.loadingBar2.width = 460;
      const self = this;
      setTimeout(() => {
        self.state.start('APILoader');
      }, 1000);
    }
    this.totalLoad += 1;
  }

  create() {
    this.auth(window.user, window.name, window.email, window.phone);
  }
}
