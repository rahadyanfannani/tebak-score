import Phaser from 'phaser-ce';
import { getAllUserGuessMatch, cancelPost } from '../lib/API';

export default class GuessBoard extends Phaser.State {
  create() {
    this.total = 0;
    getAllUserGuessMatch()
      .then(({ data }) => {
        this.userGuessMatch = data.user_guess_match_data;
        this.renderMatchData();
      });

    this.drawBackground();
    const akumulatif = this.add.button(this.world.centerX - 220, 181, 'akumulatif', this.akumulatifClick.bind(this));
    const pertandinganTerakhir = this.add.button(this.world.centerX, 181, 'pertandingan_terakhir', this.lastMatchClick.bind(this));
    const tebakanSaya = this.add.button(this.world.centerX + 220, 181, 'tebakan_saya_off');

    akumulatif.anchor.set(0.5, 0);
    pertandinganTerakhir.anchor.set(0.5, 0);
    tebakanSaya.anchor.set(0.5, 0);
  }
  tebakanSayaClick() {
    cancelPost();
    this.state.start('GuessBoard');
  }
  backClick() {
    cancelPost();
    this.game.state.start('Game');
  }
  drawBackground() {
    this.background = this.add.image(this.world.centerX, 155, 'lapangan');
    this.background.anchor.set(0.5, 0);
    this.white = this.add.graphics(0, 0);
    this.white.beginFill(0xFFFFFF);
    this.white.drawRect(0, 0, this.game.width, 155);
    this.white.endFill();

    const blueHeader = this.add.image(this.world.centerX, 170, 'header_home');
    blueHeader.anchor.set(0.5, 1);

    this.white2 = this.add.graphics(0, 0);
    this.white2.beginFill(0xFFFFFF);
    this.white2.drawRect(0, this.game.height - 90, this.game.width, 90);
    this.white2.endFill();
    this.logoFooter = this.add.image(this.world.centerX, this.game.height - 50, 'logo_footer');
    this.logoFooter.anchor.set(0.5);
    this.footerLine = this.add.image(this.world.centerX, this.game.height - 100, 'orange_line');
    this.footerLine.anchor.set(0.5);
    const margin = 50;
    const marginTop = 30;
    this.logoKiri = this.add.image(margin, marginTop, 'bni_kiri');
    this.logoKanan = this.add.image(this.game.width - margin, marginTop, 'bni_kanan');
    this.logoKanan.anchor.set(1, 0);
    const fontStyle = { font: '31px Arial', fill: '#ffffff', align: 'center' };

    this.add.text(this.world.centerX - 250, 115, 'Tebakan Saya', fontStyle);
    this.add.button(this.world.centerX - 340, 108, 'btn_back', this.backClick.bind(this));
  }
  lastMatchClick() {
    cancelPost();
    this.state.start('SingleMatchLeaderboard');
  }
  akumulatifClick() {
    cancelPost();
    this.state.start('Leaderboard');
  }
  renderMatchData() {
    if (this.userGuessMatch.length > 0) {
      this.load.onFileComplete.add(this.fileComplete, this);
    }
    let i = 0;
    this.userGuessMatch.forEach((val) => {
      if (i <= 4) {
        let background = 'bg_seri';
        if (val.result === 1) {
          background = 'bg_menang';
        } else if (val.result === 0) {
          background = 'bg_kalah';
        }
        const backImage = this.add.image(this.world.centerX, 0, background);
        backImage.y = 352 + (backImage.height * i);
        backImage.anchor.set(0.5);
        this.load.image(`team_1_logo${i}`, `http://net-api.agatelabs.com/bni-tebak-skor/${val.team_1_logo_url}`, true);
        this.load.image(`team_2_logo${i}`, `http://net-api.agatelabs.com/bni-tebak-skor/${val.team_2_logo_url}`, true);
        this.load.start();

        const fontStyle = { font: '80px Arial', fill: '#FFFFFF', align: 'center' };
        const textScore1 = this.add.text(this.world.centerX - 100, 343 + (i * 186)
          , val.user_guess_team_1_score, fontStyle);
        const textScore2 = this.add.text(this.world.centerX + 100, 343 + (i * 186)
          , val.user_guess_team_2_score, fontStyle);
        textScore1.anchor.set(0.5);
        textScore2.anchor.set(0.5);
        const teamStyle = { font: '20px Arial', fill: '#FFFFFF', align: 'center' };
        const text1 = this.add.text(this.world.centerX - 250, 430 + (i * 186)
          , val.team_1_name, teamStyle);
        const text2 = this.add.text(this.world.centerX + 250, 430 + (i * 186)
          , val.team_2_name, teamStyle);
        text1.anchor.set(0.5);
        text2.anchor.set(0.5);
        const tebakanStyle = { font: '30px Arial', fill: '#FFFFFF', align: 'center' };
        let tebakanText = '';
        if (val.result_guess === 1) {
          tebakanText = '3 PT';
        } else if (val.result_guess === 0) {
          tebakanText = '0 PT';
        }
        const tebakan = this.add.text(this.world.centerX,
          400 + (i * 186), tebakanText, tebakanStyle);
        tebakan.anchor.set(0.5);
        i += 1;
      }
    });
  }
  fileComplete(progress, cacheKey) {
    const i = cacheKey.substr(cacheKey.length - 1, 1);
    let logo;
    if (cacheKey.substr(5, 1) === '1') {
      logo = this.add.image(this.world.centerX - 250, 343 + (i * 186), `team_1_logo${i}`);
    } else {
      logo = this.add.image(this.world.centerX + 250, 343 + (i * 186), `team_2_logo${i}`);
    }
    logo.anchor.set(0.5);
    this.total += 1;
    if (this.total === this.userGuessMatch.length * 2) {
      this.load.onFileComplete.remove(this.fileComplete, this);
    }
  }
}
