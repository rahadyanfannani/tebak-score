import Phaser from 'phaser-ce';
import { getActiveMatch } from '../lib/API';

export default class APILoader extends Phaser.State {
  create() {
    getActiveMatch()
      .then(({ data }) => {
        if (data.active_match_data[0]) {
          window.activeMatchData = data.active_match_data[0];
          if (!window.activeMatchData.guessed) {
            this.game.state.start('TermCondition');
          } else {
            this.game.state.start('Game');
          }
        } else {
          this.game.state.start('Game');
        }
      });
  }
}



