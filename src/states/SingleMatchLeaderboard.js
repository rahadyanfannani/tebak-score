import Phaser from 'phaser-ce';
import { getSingleLeaderboard, cancelPost } from '../lib/API';

export default class SingleMatchLeaderboard extends Phaser.State {
  create() {
    this.drawBackground();
    this.add.button(this.world.centerX - 340, 108, 'btn_back', this.backClick.bind(this));
    const akumulatif = this.add.button(this.world.centerX - 220, 181, 'akumulatif', this.akumulatifClick.bind(this));
    const pertandinganTerakhir = this.add.button(this.world.centerX, 181, 'pertandingan_terakhir_off');
    const tebakanSaya = this.add.button(this.world.centerX + 220, 181, 'tebakan_saya', this.tebakanSayaClick.bind(this));

    akumulatif.anchor.set(0.5, 0);
    pertandinganTerakhir.anchor.set(0.5, 0);
    tebakanSaya.anchor.set(0.5, 0);
    getSingleLeaderboard().then(({ data }) => {
      this.data = data;
      this.leaderboardData = data.leaderboard_data;
      this.drawLeaderboard();
    });
    for (let i = 0; i < 10; i += 1) {
      const background = this.add.image(this.world.centerX, 300 + (80 * i), 'bg_hitam');
      background.anchor.set(0.5);
    }
    const footer = this.add.image(this.world.centerX, this.world.height - 150, 'bg_footer');
    footer.anchor.set(0.5);
  }
  akumulatifClick() {
    cancelPost();
    this.state.start('Leaderboard');
  }
  tebakanSayaClick() {
    cancelPost();
    this.state.start('GuessBoard');
  }

  drawLeaderboard() {
    let i = 0;
    this.leaderboardData.forEach((val) => {
      const fontStyle = { font: '40px Arial', fill: '#ffffff', align: 'center' };
      this.add.text(this.world.centerX - 310, 280 + (80 * i), i + 1, fontStyle);
      const subname = (val.name.length > 14) ? `${val.name.substr(0, 15)}...` : val.name;
      this.add.text(this.world.centerX - 200, 280 + (80 * i), subname, fontStyle);
      fontStyle.align = 'right';
      this.add.text(this.world.centerX + 220, 280 + (80 * i), `${(parseInt(val.score, 10) >= 1) ? '3' : '0'} PT`, fontStyle);
      i += 1;
    });
    const subname = (window.name.length > 14) ? `${window.name.substr(0, 15)}...` : window.name;
    const fontStyle = { font: '40px Arial', fill: '#ffffff', align: 'center' };
    this.add.text(this.world.centerX - 310, 1110, this.data.user_rank, fontStyle);
    this.add.text(this.world.centerX - 200, 1110, subname, fontStyle);
    this.add.text(this.world.centerX + 220, 1110, `${(parseInt(this.data.user_highest_score, 10) >= 1) ? '3' : '0'} PT`, fontStyle);
  }
  drawBackground() {
    this.background = this.add.image(this.world.centerX, 155, 'lapangan');
    this.background.anchor.set(0.5, 0);
    this.white = this.add.graphics(0, 0);
    this.white.beginFill(0xFFFFFF);
    this.white.drawRect(0, 0, this.game.width, 155);
    this.white.endFill();

    const blueHeader = this.add.image(this.world.centerX, 170, 'header_home');
    blueHeader.anchor.set(0.5, 1);

    this.white2 = this.add.graphics(0, 0);
    this.white2.beginFill(0xFFFFFF);
    this.white2.drawRect(0, this.game.height - 90, this.game.width, 90);
    this.white2.endFill();
    this.logoFooter = this.add.image(this.world.centerX, this.game.height - 50, 'logo_footer');
    this.logoFooter.anchor.set(0.5);
    this.footerLine = this.add.image(this.world.centerX, this.game.height - 100, 'orange_line');
    this.footerLine.anchor.set(0.5);
    const margin = 50;
    const marginTop = 30;
    this.add.image(margin, marginTop, 'bni_kiri');
    this.logoKanan = this.add.image(this.game.width - margin, marginTop, 'bni_kanan');
    this.logoKanan.anchor.set(1, 0);
    const fontStyle = { font: '31px Arial', fill: '#ffffff', align: 'center' };
    this.add.text(this.world.centerX - 250, 115, 'Single Match', fontStyle);
  }
  backClick() {
    cancelPost();
    this.game.state.start('Game');
  }
}
