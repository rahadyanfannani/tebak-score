import Phaser from 'phaser-ce';
import moment from 'moment';
import PhaseSlider from '../lib/phase-slide';
import { submitGuessMatch } from '../lib/API';

function toScore(data) {
  if (data < 10) {
    return `0${data}`;
  }
  return data;
}

function gotoLink(url) {
  return () => {
    window.location.href = url;
  };
}

export default class Game extends Phaser.State {
  init() {
    this.scoreA = 0;
    this.scoreB = 0;
    this.historyClick = this.historyClick.bind(this);
    this.leaderboardClick = this.leaderboardClick.bind(this);
    this.playClick = this.playClick.bind(this);
  }
  drawBackground() {
    this.background = this.add.image(this.world.centerX, 155, 'lapangan');
    this.background.anchor.set(0.5, 0);


    this.white = this.add.graphics(0, 0);
    this.white.beginFill(0xFFFFFF);
    this.white.drawRect(0, 0, this.game.width, 155);
    this.white.endFill();


    this.white2 = this.add.graphics(0, 0);
    this.white2.beginFill(0xFFFFFF);
    this.white2.drawRect(0, this.game.height - 90, this.game.width, 90);
    this.white2.endFill();

    this.logoFooter = this.add.image(this.world.centerX, this.game.height - 50, 'logo_footer');
    this.logoFooter.anchor.set(0.5);

    const margin = 50;
    const marginTop = 30;
    const blueHeader = this.add.image(this.world.centerX, 170, 'header_home');
    blueHeader.anchor.set(0.5, 1);
    const fontStyle = { font: '31px Arial', fill: '#ffffff', align: 'center' };
    this.add.text(28, 115, 'Pertandingan berikutnya', fontStyle);
    this.add.image(margin, marginTop, 'bni_kiri');
    this.logoKanan = this.add.image(this.game.width - margin, marginTop, 'bni_kanan');
    this.logoKanan.anchor.set(1, 0);

    const bgPlayer1 = this.add.image(this.world.centerX, 370, 'bg_player');
    const bgPlayer2 = this.add.image(this.world.centerX, 650, 'bg_player');
    bgPlayer1.scale.set(0.85);
    bgPlayer2.scale.set(0.85);
    bgPlayer1.anchor.set(0.5, 0);
    bgPlayer2.anchor.set(0.5, 0);

    const notif = this.add.image(this.world.centerX, 300, 'notif');
    notif.anchor.set(0.5, 0);

    const box1 = this.add.image(this.world.centerX - 33, 228, 'score');
    const box2 = this.add.image(this.world.centerX + 33, 228, 'score');
    box1.anchor.set(0.5);
    box2.anchor.set(0.5);

    const box3 = this.add.image(this.world.centerX - 33 - 180, 228, 'score');
    const box4 = this.add.image(this.world.centerX + (33 - 180), 228, 'score');
    box3.anchor.set(0.5);
    box4.anchor.set(0.5);

    const box5 = this.add.image(this.world.centerX - (33 + 180), 228, 'score');
    const box6 = this.add.image(this.world.centerX + 33 + 180, 228, 'score');
    box5.anchor.set(0.5);
    box6.anchor.set(0.5);
    const dotStyle = { font: '90px Sans-Serif', fill: '#ffffff', align: 'center' };

    this.add.text(this.world.centerX - 90 - 10, 170, ':', dotStyle);
    this.add.text(this.world.centerX + (90 - 10), 170, ':', dotStyle);

    const clockStyle = { font: '22px Arial', fill: '#ffffff', align: 'center' };
    const textHari = this.add.text(this.world.centerX - 180, 290, 'HARI', clockStyle);
    textHari.anchor.set(0.5);
    const textJam = this.add.text(this.world.centerX, 290, 'JAM', clockStyle);
    textJam.anchor.set(0.5);
    const textMenit = this.add.text(this.world.centerX + 180, 290, 'MENIT', clockStyle);
    textMenit.anchor.set(0.5);

    const vs = this.add.image(this.world.centerX, 610, 'vs');
    vs.anchor.set(0.5);
  }
  create() {
    this.drawBackground();
    this.activeMatchData = window.activeMatchData;
    const fontStyle = { font: '23px Arial', fill: '#ffffff', align: 'center' };
    this.textPengumuman = this.add.text(this.world.centerX, 330, 'Belum Ada Pertandingan', fontStyle);
    this.textPengumuman.anchor.set(0.5);

    this.slider = new PhaseSlider(this);
    const dataBanner = [];
    let height = 168;

    let scale;
    for (let i = 0; i < window.totalImage; i += 1) {
      const link = (window.banner[i].banner_link === '') ? 'ttps://www.google.co.id/' : window.banner[i].banner_link;
      const btnBanner = this.add.button(0, 0, `banner${(i + 1).toString()}`, gotoLink(link));
      scale = this.game.width / 720;
      btnBanner.scale.set(scale);
      dataBanner.push(btnBanner);
    }
    height *= scale;

    this.slider.createSlider({
      customSliderBG: false,
      mode: 'horizontal',
      sliderBGAlpha: 0.8,
      width: this.game.width,
      height,
      x: 0,
      y: this.game.height - 90 - height,
      customHandleNext: 'slider_right',
      customHandlePrev: 'slider_left',
      objects: dataBanner,
      autoAnimate: true,
    });
    this.footerLine = this.add.image(this.world.centerX, this.game.height - 100 - height, 'orange_line');
    this.footerLine.anchor.set(0.5);
    this.bannerHeight = height;
    const leaderboardBtn = this.add.button(this.world.centerX + 260, this.game.height - 150 - height, 'leaderboard', this.leaderboardClick);
    const historyBtn = this.add.button(0, this.game.height - 150 - height, 'history', this.historyClick);
    leaderboardBtn.anchor.set(0.5);
    historyBtn.anchor.set(0.5);
    leaderboardBtn.x = this.game.width - ((leaderboardBtn.width / 2) - 6);
    historyBtn.x = (historyBtn.width / 2) - 6;


    if (this.activeMatchData) {
      window.match_id = this.activeMatchData.id;
      this.textPengumuman.text = 'Tebak Skornya Sekarang!';
      this.load.image('team_1_logo', `http://net-api.agatelabs.com/bni-tebak-skor/${this.activeMatchData.team_1_logo_url}`);
      this.load.image('team_2_logo', `http://net-api.agatelabs.com/bni-tebak-skor/${this.activeMatchData.team_2_logo_url}`);
      this.load.onFileComplete.add(this.renderLogo, this);
      this.load.start();
      this.renderMatch();
    }
  }
  renderLogo(progress, cacheKey) {
    if (cacheKey === 'team_1_logo' || cacheKey === 'team_2_logo') {
      const teamName = this.add.group();
      let teamText;
      let logo;
      const teamNameStyle = { font: '35px Arial', fill: '#FFFFFF', align: 'center' };
      if (cacheKey === 'team_1_logo') {
        logo = this.add.image(0, 0, 'team_1_logo');
        teamText = this.add.text(logo.width + 10, 0, this.activeMatchData.team_1_name.toUpperCase()
          , teamNameStyle);
        teamName.y = 418;
      } else if (cacheKey === 'team_2_logo') {
        logo = this.add.image(0, 0, 'team_2_logo');
        teamText = this.add.text(logo.width + 10, 0, this.activeMatchData.team_2_name.toUpperCase()
          , teamNameStyle);
        teamName.y = 708;
      }
      logo.scale.set(0.85);

      teamName.add(logo);
      teamName.add(teamText);
      teamName.x = this.world.centerX - ((teamText.width + 10 + logo.width) / 2);
      teamText.anchor.set(0, 0.5);
      logo.anchor.set(0, 0.5);
    }
  }
  renderMatch() {
    this.calculateTime();
    let style = { font: '80px Arial', fill: '#000000', align: 'center' };


    if (!this.activeMatchData.guessed) {
      this.placeholderA = this.add.image(this.world.centerX, 525, 'editScore');
      this.placeholderB = this.add.image(this.world.centerX, 815, 'editScore');
      this.placeholderA.scale.set(0.8);
      this.placeholderB.scale.set(0.8);
      this.placeholderA.anchor.set(0.5);
      this.placeholderB.anchor.set(0.5);
      this.scoreTextA = this.add.text(this.world.centerX, 530, '00');
      this.scoreTextB = this.add.text(this.world.centerX, 820, '00');

      const arrowMargin = 40;
      const leftArrowA = this.add.image(this.world.centerX - (this.placeholderA.width / 2) - arrowMargin, this.placeholderA.y, 'left');
      leftArrowA.anchor.set(0.5);
      const rightArrowA = this.add.image(this.world.centerX + (this.placeholderA.width / 2) + arrowMargin, this.placeholderA.y, 'right');
      rightArrowA.anchor.set(0.5);
      const leftArrowB = this.add.image(this.world.centerX - (this.placeholderB.width / 2) - arrowMargin, this.placeholderB.y, 'left');
      leftArrowB.anchor.set(0.5);
      const rightArrowB = this.add.image(this.world.centerX + (this.placeholderB.width / 2) + arrowMargin, this.placeholderB.y, 'right');
      rightArrowB.anchor.set(0.5);


      leftArrowA.direction = 'left';
      leftArrowA.team = 'A';
      rightArrowA.direction = 'right';
      rightArrowA.team = 'A';

      leftArrowB.direction = 'left';
      leftArrowB.team = 'B';
      rightArrowB.direction = 'right';
      rightArrowB.team = 'B';


      leftArrowA.inputEnabled = true;
      rightArrowA.inputEnabled = true;
      leftArrowB.inputEnabled = true;
      rightArrowB.inputEnabled = true;
      leftArrowA.events.onInputDown.add(this.arrowClick, this);
      rightArrowA.events.onInputDown.add(this.arrowClick, this);
      leftArrowB.events.onInputDown.add(this.arrowClick, this);
      rightArrowB.events.onInputDown.add(this.arrowClick, this);

      const play = this.add.button(this.world.centerX, this.game.height - 150 - this.bannerHeight, 'submit', this.playClick);
      play.anchor.set(0.5);
    } else {
      style = { font: '80px Arial', fill: '#FFFFFF', align: 'center' };
      this.placeholderA = this.add.image(this.world.centerX, 525, 'bgtampilscore');
      this.placeholderB = this.add.image(this.world.centerX, 815, 'bgtampilscore');
      this.placeholderA.scale.set(0.8);
      this.placeholderB.scale.set(0.8);
      this.placeholderA.anchor.set(0.5);
      this.placeholderB.anchor.set(0.5);
      this.scoreTextA = this.add.text(this.world.centerX, 530, '00');
      this.scoreTextB = this.add.text(this.world.centerX, 820, '00');
      this.scoreTextA.text = toScore(this.activeMatchData.team_1_guess);
      this.scoreTextB.text = toScore(this.activeMatchData.team_2_guess);
      this.textPengumuman.text = 'Skor anda sudah disubmit. Tunggu hingga usai pertandingan';
    }
    this.scoreTextA.setStyle(style);
    this.scoreTextB.setStyle(style);
    this.scoreTextA.anchor.set(0.5);
    this.scoreTextB.anchor.set(0.5);
    const countdown = this.getDuration();

    const timeStyle = { font: '80px Arial', fill: '#FFFFFF', align: 'center' };
    const timePadding = 150;
    const letterPadding = 65;
    const marginTop = 235;

    this.day1Text = this.add.text(this.world.centerX - timePadding - letterPadding
      , marginTop, countdown.days[0], timeStyle);
    this.day2Text = this.add.text(this.world.centerX - timePadding
      , marginTop, countdown.days[1], timeStyle);
    this.hour1Text = this.add.text(this.world.centerX - (letterPadding / 2)
      , marginTop, countdown.hours[0], timeStyle);
    this.hour2Text = this.add.text(this.world.centerX + (letterPadding / 2)
      , marginTop, countdown.hours[1], timeStyle);
    this.minute1Text = this.add.text(this.world.centerX + timePadding
      , marginTop, countdown.minutes[0], timeStyle);
    this.minute2Text = this.add.text(this.world.centerX + timePadding + letterPadding
      , marginTop, countdown.minutes[1], timeStyle);

    this.day1Text.anchor.set(0.5);
    this.day2Text.anchor.set(0.5);
    this.hour1Text.anchor.set(0.5);
    this.hour2Text.anchor.set(0.5);
    this.minute1Text.anchor.set(0.5);
    this.minute2Text.anchor.set(0.5);

    this.time.events.loop(Phaser.Timer.MINUTE, this.updateDuration, this);
    this.time.events.start();
  }

  calculateTime() {
    this.matchTime = moment(this.activeMatchData.match_time);
    this.currentTime = moment();
    this.duration = moment.duration(this.matchTime.diff(this.currentTime));
    if (this.duration.asMinutes <= 0) {
      this.state.start('APILoader');
    }
  }


  updateDuration() {
    this.calculateTime();
    const countdown = this.getDuration();


    this.day1Text.text = countdown.days[0];
    this.day2Text.text = countdown.days[1];
    this.hour1Text.text = countdown.hours[0];
    this.hour2Text.text = countdown.hours[1];
    this.minute1Text.text = countdown.minutes[0];
    this.minute2Text.text = countdown.minutes[1];
  }
  getDuration() {
    return {
      days: (this.duration.days() <= 9) ? `0${this.duration.days()}` : this.duration.days().toString(),
      hours: (this.duration.hours() <= 9) ? `0${this.duration.hours()}` : this.duration.hours().toString(),
      minutes: (this.duration.minutes() <= 9) ? `0${this.duration.minutes()}` : this.duration.minutes().toString(),
    };
  }
  playClick() {
    this.black = this.add.graphics(0, 0);
    this.black.beginFill(0x000000);
    this.black.drawRect(0, 0, this.game.width, this.game.height);
    this.black.endFill();
    this.black.alpha = 0.5;
    this.popup = this.add.image(this.world.centerX, this.world.centerY - 100, 'popup_choice');
    const fontStyle = { font: '30px Arial', fill: '#000000', align: 'center' };
    this.popuptext = this.add.text(this.world.centerX, this.world.centerY - 150, 'Apakah anda yakin dengan tebakan anda ?', fontStyle);
    this.popup.anchor.set(0.5);
    this.popuptext.anchor.set(0.5);
    if (!this.btnYes) {
      this.btnYes = new Phaser.Rectangle(this.world.centerX - 250, 650, 200, 50);
      this.btnNo = new Phaser.Rectangle(this.world.centerX + 50, 650, 200, 50);
    }

    this.handlePointerDown = function handlePointerDown(pointer) {
      if (this.btnYes.contains(pointer.x, pointer.y)) {
        this.yesClick();
      } else if (this.btnNo.contains(pointer.x, pointer.y)) {
        this.noClick();
      }
    }.bind(this);

    this.input.onDown.add(this.handlePointerDown);
  }
  yesClick() {
    submitGuessMatch(this.scoreA, this.scoreB).then(({ data }) => {
      if (data.submit_guess_match_status) {
        this.closePopup();
        setTimeout(() => {
          this.state.start('APILoader');
        }, 1000);
        this.black = this.add.graphics(0, 0);
        this.black.beginFill(0x000000);
        this.black.drawRect(0, 0, this.game.width, this.game.height);
        this.black.endFill();
        this.black.alpha = 0.5;
      } else {
        this.closePopup();
        this.showPopup('Terjadi kesalahan saat submit, \nsilakan ulangi lagi');
      }
    });
  }

  noClick() {
    this.closePopup();
  }
  showPopup(msg) {
    this.black = this.add.graphics(0, 0);
    this.black.beginFill(0x000000);
    this.black.drawRect(0, 0, this.game.width, this.game.height);
    this.black.endFill();
    this.black.alpha = 0.5;
    this.popup = this.add.image(this.world.centerX, this.world.centerY - 100, 'notif_popup');
    const fontStyle = { font: '40px Arial', fill: '#000000', align: 'center' };
    this.popuptext = this.add.text(this.world.centerX, this.world.centerY - 100, msg, fontStyle);
    this.popup.anchor.set(0.5);
    this.popuptext.anchor.set(0.5);
    this.black.inputEnabled = true;
    this.popup.inputEnabled = true;
    this.black.events.onInputDown.add(this.closeNotif.bind(this));
    this.popup.events.onInputDown.add(this.closeNotif.bind(this));
  }
  closeNotif() {
    this.black.destroy();
    this.popup.destroy();
    this.popuptext.destroy();
  }
  closePopup() {
    this.black.destroy();
    this.popup.destroy();
    this.input.onDown.remove(this.handlePointerDown);
    this.popuptext.destroy();
  }

  leaderboardClick() {
    this.state.start('Leaderboard');
  }
  historyClick() {
    this.state.start('ScoreHistory');
  }
  arrowClick(obj) {
    const tmp = (obj.direction === 'left') ? -1 : 1;

    if (obj.team === 'A') {
      this.scoreA += tmp;
      this.scoreA = (this.scoreA < 0) ? 0 : this.scoreA;
      this.scoreTextA.text = toScore(this.scoreA);
    } else {
      this.scoreB += tmp;
      this.scoreB = (this.scoreB < 0) ? 0 : this.scoreB;
      this.scoreTextB.text = toScore(this.scoreB);
    }
  }
  resumed() {
    if (this.activeMatchData) {
      this.calculateTime();
      this.updateDuration();
    }
  }
}
