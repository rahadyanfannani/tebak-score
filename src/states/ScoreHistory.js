import Phaser from 'phaser-ce';
import moment from 'moment';
import { getAllMatch, cancelPost } from '../lib/API';

export default class ScoreHistory extends Phaser.State {
  init() {
    this.total = 0;
  }
  create() {
    this.drawBackground();
    this.add.button(this.world.centerX - 340, 108, 'btn_back', this.backClick.bind(this));
    getAllMatch().then(({ data }) => {
      console.log(data.match_data);
      this.matchData = data.match_data;
      this.renderMatchData();
    });
  }
  drawBackground() {
    this.background = this.add.image(this.world.centerX, 155, 'lapangan');
    this.background.anchor.set(0.5, 0);
    this.white = this.add.graphics(0, 0);
    this.white.beginFill(0xFFFFFF);
    this.white.drawRect(0, 0, this.game.width, 155);
    this.white.endFill();

    const blueHeader = this.add.image(this.world.centerX, 170, 'header_home');
    blueHeader.anchor.set(0.5, 1);
    this.white2 = this.add.graphics(0, 0);
    this.white2.beginFill(0xFFFFFF);
    this.white2.drawRect(0, this.game.height - 90, this.game.width, 90);
    this.white2.endFill();

    this.logoFooter = this.add.image(this.world.centerX, this.game.height - 50, 'logo_footer');
    this.logoFooter.anchor.set(0.5);

    this.footerLine = this.add.image(this.world.centerX, this.game.height - 100, 'orange_line');
    this.footerLine.anchor.set(0.5);


    const margin = 50;
    const marginTop = 30;
    this.add.image(margin, marginTop, 'bni_kiri');
    this.logoKanan = this.add.image(this.game.width - margin, marginTop, 'bni_kanan');
    this.logoKanan.anchor.set(1, 0);
    const fontStyle = { font: '27px Arial', fill: '#ffffff', align: 'center' };
    this.add.text(this.world.centerX - 250, 115, 'Hasil Pertandingan', fontStyle);
  }
  renderMatchData() {
    this.load.onFileComplete.add(this.fileComplete, this);
    let i = 0;
    this.matchData.reverse();
    this.matchData.forEach((val) => {
      let background = 'bg_seri';
      if (val.result === 1) {
        background = 'bg_menang';
      } else if (val.result === 2) {
        background = 'bg_kalah';
      }
      const backImage = this.add.image(this.world.centerX, 0, background);
      backImage.y = 265 + (backImage.height * i);
      backImage.anchor.set(0.5);

      this.load.image(`team_1_logo${i}`, `http://net-api.agatelabs.com/bni-tebak-skor/${val.team_1_logo_url}`, true);
      this.load.image(`team_2_logo${i}`, `http://net-api.agatelabs.com/bni-tebak-skor/${val.team_2_logo_url}`, true);
      this.load.start();

      const fontStyle = { font: '80px Arial', fill: '#FFFFFF', align: 'center' };
      const textScore1 = this.add.text(this.world.centerX - 100, 253 + (i * 186)
        , val.team_1_score, fontStyle);
      const textScore2 = this.add.text(this.world.centerX + 100, 253 + (i * 186)
        , val.team_2_score, fontStyle);
      textScore1.anchor.set(0.5);
      textScore2.anchor.set(0.5);
      const teamStyle = { font: '20px Arial', fill: '#FFFFFF', align: 'center' };
      const text1 = this.add.text(this.world.centerX - 250
        , 320 + (i * 186), val.team_1_name, teamStyle);
      const text2 = this.add.text(this.world.centerX + 250
        , 320 + (i * 186), val.team_2_name, teamStyle);
      text1.anchor.set(0.5);
      text2.anchor.set(0.5);
      const matchDate = moment(val.match_time);
      const textDate = this.add.text(this.world.centerX - 5, 325 + (i * 186), matchDate.format('DD MMM YYYY'), teamStyle);
      textDate.anchor.set(1, 0);
      i += 1;
    });
  }
  fileComplete(progress, cacheKey) {
    const i = cacheKey.substr(cacheKey.length - 1, 1);
    let logo;
    if (cacheKey.substr(5, 1) === '1') {
      logo = this.add.image(this.world.centerX - 250, 253 + (i * 186), `team_1_logo${i}`);
    } else {
      logo = this.add.image(this.world.centerX + 250, 253 + (i * 186), `team_2_logo${i}`);
    }
    logo.anchor.set(0.5);
    this.total += 1;

    if (this.total === this.matchData.length * 2) {
      this.load.onFileComplete.remove(this.fileComplete, this);
    }
  }
  backClick() {
    cancelPost();
    this.game.state.start('Game');
  }
}
