import Phaser from 'phaser-ce';

export default class TermCondition extends Phaser.State {
  init() {
    this.state = false;
  }
  create() {
    this.drawBackground();

    this.persyaratan = this.add.image(this.world.centerX, 160, 'persyaratan');
    this.persyaratan.anchor.set(0.5, 0);


    this.checkbox = this.add.button(this.world.centerX - 275, 1085, 'checkbox_false', this.checkboxClick.bind(this));
    this.checkbox.anchor.set(0.5);
    this.checkbox.scale.set(0.6);


    this.disable = this.add.graphics(0, 0);
    this.disable.beginFill(0xF5F5F5);
    this.disable.drawRect(this.world.centerX + 145, 1105, 160, 50);
    this.disable.endFill();
    this.lanjut = new Phaser.Rectangle(this.world.centerX + 120, 1100, 200, 80);
    this.handlePointerDown = function handlePointerDown(pointer) {
      if (this.lanjut.contains(pointer.x, pointer.y)) {
        this.lanjutClick();
      }
    };
    this.handlePointerDown = this.handlePointerDown.bind(this);

    this.input.onDown.add(this.handlePointerDown);
  }
  lanjutClick() {
    if (this.state) {
      this.game.state.start('Game');
    }
  }

  drawBackground() {
    this.background = this.add.image(this.world.centerX, 155, 'lapangan');
    this.background.anchor.set(0.5, 0);

    this.white = this.add.graphics(0, 0);
    this.white.beginFill(0xFFFFFF);
    this.white.drawRect(0, 0, this.game.width, 155);
    this.white.endFill();

    const blueHeader = this.add.image(this.world.centerX, 170, 'header_home');
    blueHeader.anchor.set(0.5, 1);


    this.white2 = this.add.graphics(0, 0);
    this.white2.beginFill(0xFFFFFF);
    this.white2.drawRect(0, this.game.height - 90, this.game.width, 90);
    this.white2.endFill();

    this.logoFooter = this.add.image(this.world.centerX, this.game.height - 50, 'logo_footer');
    this.logoFooter.anchor.set(0.5);

    this.footerLine = this.add.image(this.world.centerX, this.game.height - 100, 'orange_line');
    this.footerLine.anchor.set(0.5);


    const margin = 50;
    const marginTop = 30;
    this.add.image(margin, marginTop, 'bni_kiri');
    this.logoKanan = this.add.image(this.game.width - margin, marginTop, 'bni_kanan');
    this.logoKanan.anchor.set(1, 0);
    const fontStyle = { font: '31px Arial', fill: '#ffffff', align: 'center' };

    this.add.text(this.world.centerX - 200, 115, 'Tebak Skor', fontStyle);
  }
  checkboxClick() {
    const texture = (this.state) ? 'checkbox_false' : 'checkbox';
    this.checkbox.loadTexture(texture);
    this.state = !this.state;
    this.disable.visible = !this.disable.visible;
  }
}
