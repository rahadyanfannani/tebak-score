import Phaser from 'phaser-ce';

export default class Boot extends Phaser.State {
  preload() {
    this.load.image('lapangan', './assets/images/pieces/background.jpg');
    this.load.image('icon', './assets/images/icon.png');
    this.load.image('loading_bar', './assets/images/loading_bar.png');
    this.load.image('loading', './assets/images/loading.png');
  }
  create() {
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    this.scale.pageAlignHorizontally = true;
    this.scale.pageAlignVertically = true;
    this.state.start('Preload');
  }
}
