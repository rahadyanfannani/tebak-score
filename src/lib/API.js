import axios from 'axios';

const baseUrl = 'http://net-api.agatelabs.com/bni-tebak-skor/connector/connector.php';
let CancelToken = axios.CancelToken;
let source = CancelToken.source();

export default class API {
  static post(data) {
    CancelToken = axios.CancelToken;
    source = CancelToken.source();
    const formData = new FormData();
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key]);
    });
    return axios.post(baseUrl, formData, { cancelToken: source.token });
  }
  static login(email, userEmail, userPhone, name) {
    return API.post({
      email,
      login_type: 1,
      mode: 'login',
      user_email: userEmail,
      user_phone: userPhone,
      name,
    });
  }
  static register(email, name) {
    return API.post({ email, name, mode: 'register' });
  }

  static getActiveMatch() {
    return API.post({
      mode: 'getactivematch',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static getLeaderboard() {
    return API.post({
      mode: 'getleaderboard',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static getSingleLeaderboard() {
    return API.post({
      mode: 'getlatestleaderboard',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static getAllMatch() {
    return API.post({
      mode: 'getallmatch',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static submitGuessMatch(team1Score, team2Score) {
    return API.post({
      mode: 'submitguessmatch',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
      match_id: window.match_id,
      version: 1,
      team_1_score: team1Score,
      team_2_score: team2Score,
    });
  }
  static getAllUserGuessMatch() {
    return API.post({
      mode: 'getalluserguessmatch',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static getUserGuessMatch() {
    return API.post({
      mode: 'getuserguessmatch',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
      match_id: window.match_id,
    });
  }
  static getBanner() {
    return API.post({
      mode: 'getbanner',
      session_id: window.session_id,
      email: window.user,
      user_id: window.user_id,
      leaderboard_id: 1,
    });
  }
  static cancelPost() {
    source.cancel();
  }
}
export const post = API.post;
export const getActiveMatch = API.getActiveMatch;
export const getLeaderboard = API.getLeaderboard;
export const getSingleLeaderboard = API.getSingleLeaderboard;
export const getAllMatch = API.getAllMatch;
export const submitGuessMatch = API.submitGuessMatch;
export const getAllUserGuessMatch = API.getAllUserGuessMatch;
export const getUserGuessMatch = API.getUserGuessMatch;
export const getBanner = API.getBanner;
export const login = API.login;
export const register = API.register;
export const cancelPost = API.cancelPost;
