
import Phaser from 'phaser-ce';
import Raven from 'raven-js';
import GameState from './states/Game';
import BootState from './states/Boot';
import PreloadState from './states/Preload';
import ScoreHistoryState from './states/ScoreHistory';
import LeaderboardState from './states/Leaderboard';
import TermConditionState from './states/TermCondition';
import APILoaderState from './states/APILoader';
import GuessBoardState from './states/GuessBoard';
import SingleMatchLeaderboardState from './states/SingleMatchLeaderboard';

class Game extends Phaser.Game {
  constructor() {
    super(1280 * (window.innerWidth / window.innerHeight), 1280, Phaser.CANVAS, 'content'); // 10 : 18
    this.state.add('Preload', PreloadState);
    this.state.add('Boot', BootState);
    this.state.add('Game', GameState);
    this.state.add('ScoreHistory', ScoreHistoryState);
    this.state.add('Leaderboard', LeaderboardState);
    this.state.add('TermCondition', TermConditionState);
    this.state.add('GuessBoard', GuessBoardState);
    this.state.add('APILoader', APILoaderState);
    this.state.add('SingleMatchLeaderboard', SingleMatchLeaderboardState);
    window.totalScore = 0;
    this.state.start('Boot');
  }
}

Raven
  .config('https://8b530b3c35b749cc89ecd34ae3c80103@sentry.io/196821')
  .install();

document.addEventListener('DOMContentLoaded', () => {
  const urlParams = new URLSearchParams(window.location.search);
  if (urlParams.has('user_id') && urlParams.has('name') && urlParams.has('email') && urlParams.has('phone')) {
    window.user = urlParams.get('user_id');
    window.name = urlParams.get('name');
    window.email = urlParams.get('email');
    window.phone = urlParams.get('phone');
    Raven.setUserContext({
      id: urlParams.get('user_id'),
      username: urlParams.get('name'),
      email: urlParams.get('email'),
      phone: urlParams.get('phone'),
    });
    window.game = new Game();
  } else {
    const link = "<a href='https://itunes.apple.com/id/app/bni-experience/id964706797?mt=8'><img width='150' src='./assets/images/appstore.png'></a><br>" +
            "<a href='https://play.google.com/store/apps/details?id=com.bilinedev.bniexperience'><img width='170' src='./assets/images/playstore.png'></a>";

    const content = document.getElementById('content');
    document.body.style.backgroundColor = '#ffffff';
    content.innerHTML = `<div align='center' class='blokir'><p><b>Maaf, Game hanya bisa di akses melalui apps BNI Experience</b></p>${
      link}</div>`;
    content.style.backgroundColor = '#ffffff';
  }
});
